data "ibm_container_cluster_config" "cluster_config" {
  cluster_name_id   = var.cluster_id

  # Required for getting the calico configuration
  admin           = "true"
  network         = "true"
  config_dir      = "/tmp"
}

resource "null_resource" "delete-deployment-topaz-consultas" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/delete_elements.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "deployment,service" "topaz-consultas"
        EOT
        on_failure = continue
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-job-yaml-topaz-consultas" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-consultas.yaml"
        EOT
    }
    depends_on = [ null_resource.delete-deployment-topaz-consultas ]
}

resource "null_resource" "delete-deployment-topaz-transacciones" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/delete_elements.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "deployment,service" "topaz-transacciones"
        EOT
        on_failure = continue
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-job-yaml-topaz-transacciones" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-transacciones.yaml"
        EOT
    }
    depends_on = [ null_resource.delete-deployment-topaz-transacciones ]
}